package com.example.coinstats.repository

import com.example.coinstats.data.db.currency.CurrencyData
import com.example.coinstats.data.network.model.DailyExRates
import com.example.coinstats.useCase.CurrencyRemote
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import org.koin.standalone.KoinComponent


class CurrencyRepository(currencyRemote: CurrencyRemote) :
    ICurrencyRepository,
    KoinComponent {

    override fun getTodayYesterdayCurrencies(): Observable<MutableList<CurrencyData>> {
        return Observable.zip(
            todayCurr, yesterdayCurr,
            BiFunction<DailyExRates, DailyExRates, MutableList<CurrencyData>> { t1, t2 ->
                createTYList(t1, t2)
            }).subscribeOn(Schedulers.io())
    }


    override fun getTodayTomorrowCurrencies(): Observable<MutableList<CurrencyData>> {
        return Observable.zip(
            todayCurr, tomorrowCurr,
            BiFunction<DailyExRates, DailyExRates, MutableList<CurrencyData>> { t1, t2 ->
                createTMList(t1, t2)
            }).subscribeOn(Schedulers.io())
    }

    private fun createTYList(today: DailyExRates, yesterday: DailyExRates): MutableList<CurrencyData> {
        var currencyList: MutableList<CurrencyData> = emptyList<CurrencyData>().toMutableList()
        return if (today.currency == null) {
            currencyList
        } else if (today.currency == null && yesterday.currency == null) {
            currencyList
        } else {
            for (i in 0 until today.currency!!.size) {
                currencyList = mutableListOf()
                val data = CurrencyData()
                data.shortName = today.currency!![i].charCode
                data.fullName = today.currency!![i].name
                data.scale = today.currency!![i].scale
                data.firstPrice = yesterday.currency!![i].rate
                data.secondPrice = today.currency!![i].rate
                currencyList.add(i, data)
            }
            currencyList
        }
    }

    private fun createTMList(today: DailyExRates, tomorrow: DailyExRates): MutableList<CurrencyData> {
        var currencyList: MutableList<CurrencyData> = emptyList<CurrencyData>().toMutableList()
        return if (today.currency == null) {
            currencyList
        } else if (today.currency == null && tomorrow.currency == null) {
            currencyList
        } else {
            for (i in 0 until today.currency!!.size) {
                val data = CurrencyData()
                data.shortName = today.currency!![i].charCode
                data.fullName = today.currency!![i].name
                data.scale = today.currency!![i].scale
                data.firstPrice = today.currency!![i].rate
                data.secondPrice = tomorrow.currency!![i].rate
                currencyList.add(i, data)
            }
            currencyList
        }
    }

    var todayCurr = currencyRemote.getTodayCurrencyPrices()
    var tomorrowCurr = currencyRemote.getTomorrowCurrencyPrices()
    var yesterdayCurr = currencyRemote.getYesterdayCurrencyPrices()
}