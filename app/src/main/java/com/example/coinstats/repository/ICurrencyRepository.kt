package com.example.coinstats.repository

import com.example.coinstats.data.db.currency.CurrencyData
import io.reactivex.Observable

interface ICurrencyRepository {

    fun getTodayTomorrowCurrencies(): Observable<MutableList<CurrencyData>>

    fun getTodayYesterdayCurrencies(): Observable<MutableList<CurrencyData>>

}