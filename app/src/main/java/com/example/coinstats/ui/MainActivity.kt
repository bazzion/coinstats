package com.example.coinstats.ui

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.coinstats.data.db.currency.CurrencyData
import com.example.coinstats.repository.CurrencyRepository
import com.example.coinstats.useCase.CurrencyRemote


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.coinstats.R.layout.activity_currency_list_container)

        getCurr()
    }

    @SuppressLint("CheckResult")
    fun getCurr() {


        val repository = CurrencyRepository(CurrencyRemote())



        val data = repository.getTodayTomorrowCurrencies()
        data.subscribe { list -> showList(list)}

    }

    private fun showList(list: List<CurrencyData>?) {

        var t = list
        return
    }

}
