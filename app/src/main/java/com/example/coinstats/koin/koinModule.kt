package com.example.coinstats.koin

import com.example.coinstats.data.network.NetworkClient
import com.example.coinstats.features.GetDate
import org.koin.dsl.module.module


// just declare it
val myModule = module {
    single { NetworkClient() }
    single { GetDate() }
}
