package com.example.coinstats.useCase

import com.example.coinstats.data.db.currency.CurrencyData
import com.example.coinstats.data.db.currency.CurrencyDataDao
import io.reactivex.Observable

class CurrencyLocal(private val currencyDataDao: CurrencyDataDao) : ICurrencyLocal {


    override fun getCurrencyList(): Observable<List<CurrencyData>> {
        return Observable.fromCallable<List<CurrencyData>> {
            currencyDataDao.getAll()
        }
    }

    override fun addAllCurrency(currencies: List<CurrencyData>) {
        currencyDataDao.insert(currencies)
    }

    override fun updateCurrency(currencies: List<CurrencyData>) {
        currencyDataDao.update(currencies)
    }
}