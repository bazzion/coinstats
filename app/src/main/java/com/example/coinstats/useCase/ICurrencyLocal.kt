package com.example.coinstats.useCase

import com.example.coinstats.data.db.currency.CurrencyData
import io.reactivex.Observable

interface ICurrencyLocal {

    fun getCurrencyList(): Observable<List<CurrencyData>>

    fun addAllCurrency(currencies: List<CurrencyData>)

    fun updateCurrency(currencies: List<CurrencyData>)

}