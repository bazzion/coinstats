package com.example.coinstats.useCase

import com.example.coinstats.data.network.ApiCurrency
import com.example.coinstats.data.network.NetworkClient
import com.example.coinstats.data.network.model.DailyExRates
import com.example.coinstats.features.GetDate
import io.reactivex.Observable
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class CurrencyRemote : ICurrencyRemote, KoinComponent {

    private val client: NetworkClient by inject()
    private val date: GetDate by inject()

    private val todayString: String = "today"
    private val tomorrowString: String = "tomorrow"
    private val yesterdayString: String = "yesterday"

//    override fun getTodayCurrencyPrices(): Observable<DailyExRates> {
//        return client.getClient(ApiCurrency::class.java, client.baseUrl)
//            .getDailyCurrency(date.getDateString(todayString))
//    }
//
//    override fun getTomorrowCurrencyPrices(): Observable<DailyExRates> {
//        return client.getClient(ApiCurrency::class.java, client.baseUrl)
//            .getDailyCurrency(date.getDateString(tomorrowString))
//    }

    override fun getTodayCurrencyPrices(): Observable<DailyExRates> {
        return client.getClient(ApiCurrency::class.java, client.baseUrl)
            .getDailyCurrency(date.getDateString(todayString))
    }

    override fun getTomorrowCurrencyPrices(): Observable<DailyExRates> {
        return client.getClient(ApiCurrency::class.java, client.baseUrl)
            .getDailyCurrency(date.getDateString(tomorrowString))
    }

    override fun getYesterdayCurrencyPrices(): Observable<DailyExRates> {
        return client.getClient(ApiCurrency::class.java, client.baseUrl)
            .getDailyCurrency(date.getDateString(yesterdayString))
    }
}