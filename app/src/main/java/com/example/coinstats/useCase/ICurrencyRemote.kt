package com.example.coinstats.useCase

import com.example.coinstats.data.network.model.DailyExRates
import io.reactivex.Observable

interface ICurrencyRemote {

    fun getTodayCurrencyPrices(): Observable<DailyExRates>

    fun getTomorrowCurrencyPrices(): Observable<DailyExRates>

    fun getYesterdayCurrencyPrices(): Observable<DailyExRates>
}