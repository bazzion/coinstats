package com.example.coinstats.data.db.currency

import androidx.room.Database
import androidx.room.RoomDatabase


@Database(entities = [CurrencyData::class], version = 1, exportSchema = false)
abstract class CurrencyDataBase : RoomDatabase() {

    abstract fun currencyDao(): CurrencyDataDao
}