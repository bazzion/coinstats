package com.example.coinstats.data.network.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "DailyExRates")
data class DailyExRates(
    @field:ElementList(inline = true, required = false)
    @param:ElementList(inline = true, required = false)
    var currency: List<Currency>? = null,

    @field:Attribute(name = "Date", required = false)
    @param:Attribute(name = "Date", required = false)
    var date: String? = null
) {

    override fun toString(): String {
        return "ClassPojo [Currency = $currency, Date = $date]"
    }
}