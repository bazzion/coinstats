package com.example.coinstats.data.db.currency

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "currencyData")
data class CurrencyData(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "short_name") var shortName: String?,
    @ColumnInfo(name = "full_name") var fullName: String?,
    @ColumnInfo(name = "first_price") var firstPrice: String?,
    @ColumnInfo(name = "second_price") var secondPrice: String?,
    @ColumnInfo(name = "scale") var scale: String?

) {
    constructor() : this(
        null, "", "", "", "", ""
    )
}