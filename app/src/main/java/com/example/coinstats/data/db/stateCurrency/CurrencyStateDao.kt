package com.example.coinstats.data.db.stateCurrency

import androidx.room.*

interface CurrencyStateDao {
    @Query("SELECT * from currencyState")
    fun getAll(): List<CurrencyState>

    @Query("SELECT * FROM currencyState WHERE id = :id")
    fun getById(id: Long): CurrencyState

    @Query("SELECT * FROM currencyData WHERE short_name == :name")
    fun getCurrencyStateBySName(name: String): CurrencyState

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(currencyState: List<CurrencyState>)

    @Update
    fun update(currencyState: List<CurrencyState>)

    @Delete
    fun deleteAll()
}