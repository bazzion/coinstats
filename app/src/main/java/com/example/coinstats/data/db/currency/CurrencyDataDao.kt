package com.example.coinstats.data.db.currency

import androidx.room.*

interface CurrencyDataDao {
    @Query("SELECT * from currencyData")
    fun getAll(): List<CurrencyData>

    @Query("SELECT * FROM currencyData WHERE id = :id")
    fun getById(id: Long): CurrencyData

    @Query("SELECT * FROM currencyData WHERE short_name == :name")
    fun getCurrencyBySName(name: String): CurrencyData

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(currencyData: List<CurrencyData>)

    @Update
    fun update(currencyData: List<CurrencyData>)

    @Delete
    fun deleteAll()
}