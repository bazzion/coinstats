package com.example.coinstats.data.network

import com.example.coinstats.data.network.model.DailyExRates
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiCurrency {

    @GET("/Services/XmlExRates.aspx")
    fun getDailyCurrency(@Query("ondate") date: String): Observable<DailyExRates>
}