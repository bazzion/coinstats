package com.example.coinstats.data.network.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "Currency")
data class Currency (
    @field:Element(name = "CharCode")
    @param:Element(name = "CharCode")
    var charCode: String? = null,

    @field:Element(name = "Rate")
    @param:Element(name = "Rate")
    var rate: String? = null,

    @field:Element(name = "Scale")
    @param:Element(name = "Scale")
    var scale: String? = null,

    @field:Attribute(name = "Id")
    @param:Attribute(name = "Id")
    var id: String? = null,

    @field:Element(name = "NumCode")
    @param:Element(name = "NumCode")
    var numCode: String? = null,

    @field:Element(name = "Name")
    @param:Element(name = "Name")
    var name: String? = null) {

    override fun toString(): String {
        return "ClassPojo [CharCode = $charCode, Rate = $rate, Scale = $scale, Id = $id, NumCode = $numCode, Name = $name]"
    }
}