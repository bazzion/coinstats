package com.example.coinstats.data.db.stateCurrency

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "currencyState")
data class CurrencyState(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    @ColumnInfo(name = "short_name") var shortName: String,
    @ColumnInfo(name = "enable") var enable: Boolean,
    @ColumnInfo(name = "position") var position: Int

) {
    constructor() : this(
        null, "", true, 0
    )
}