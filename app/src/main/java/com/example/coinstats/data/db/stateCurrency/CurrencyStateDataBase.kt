package com.example.coinstats.data.db.stateCurrency

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.coinstats.data.db.currency.CurrencyData

@Database(entities = [CurrencyData::class], version = 1, exportSchema = false)
abstract class CurrencyStateDataBase : RoomDatabase() {

    abstract fun currencyStateDao(): CurrencyStateDao
}