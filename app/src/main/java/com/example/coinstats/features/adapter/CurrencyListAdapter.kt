package com.example.coinstats.features.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.coinstats.R
import com.example.coinstats.data.db.currency.CurrencyData

class CurrencyListAdapter(private val item: List<CurrencyData>, private val context: Context) :
    RecyclerView.Adapter<ViewHolder>() {

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_currency, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    //
}