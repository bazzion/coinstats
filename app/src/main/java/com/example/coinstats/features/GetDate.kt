package com.example.coinstats.features

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*


class GetDate {

    private val todayString: String = "today"
    private val tomorrowString: String = "tomorrow"
    private val yesterdayString: String = "yesterday"

    private fun yesterday(): Date {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DATE, -1)
        return cal.time
    }

    private fun today(): Date {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DATE, 0)
        return cal.time
    }

    private fun tomorrow(): Date {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DATE, 1)
        return cal.time
    }

    @SuppressLint("SimpleDateFormat")
    fun getDateString(day: String): String {
        val dateFormat = SimpleDateFormat("MM/dd/yyyy")
        return when (day) {
            todayString -> dateFormat.format(today())
            tomorrowString -> dateFormat.format(tomorrow())
            yesterdayString -> dateFormat.format(yesterday())
            else -> {
                "00.00.0000"
            }
        }
    }
}