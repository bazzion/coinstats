package com.example.coinstats

import android.app.Application
import com.example.coinstats.koin.myModule
import org.koin.android.ext.android.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(myModule))
    }
}